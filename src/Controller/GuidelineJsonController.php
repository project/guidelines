<?php

namespace Drupal\guidelines\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\guidelines\Entity\Guideline;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class GuidelineJsonController.
 *
 *  Returns responses for Guideline routes.
 */
class GuidelineJsonController extends ControllerBase {

  /**
   * The Entity Type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new entity.
   *
   * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   This is pointing to the object of enitytype manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager'),
    );
  }

  /**
   * Return guidelines for a form.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function getFormGuidelines($entity_type, $bundle) {
    $descriptions = [];

    /** @var Drupal\guidelines\Entity\Guideline[] $guidelines */
    $guidelines = Guideline::loadByEntity($entity_type);

    foreach ($guidelines as $guideline) {
      foreach ($guideline->field_field as $field) {
        [, $b, $f] = explode('.', $field->value);
        if (!empty($bundle) && $bundle === $b) {
          $view_builder = $this->entityTypeManager()->getViewBuilder('guideline');
          $pre_render = $view_builder->view($guideline, 'default');
          $render_output = \Drupal::service('renderer')->render($pre_render);

          if (!empty($guideline->field_title->value)) {
            $title = $guideline->field_title->value;
          }
          else {
            $title = $guideline->label();
          }

          $description = [
            'label' => $f,
            'title' => $title,
            'content' => $render_output,
            'link' => $guideline->toUrl()->toString(),
          ];

          // Allow other modules to add extra fields.
          $module_handler = $this->moduleHandler();
          $context = [
            'entity_type' => $entity_type,
            'bundle' => $bundle,
          ];
          $module_handler->alter('guideline_json_fields', $description, $guideline, $context);

          if (isset($description['label']) && !empty($description['label'])) {
            $descriptions[] = $description;
          }
        }
      }
    }

    return new JsonResponse($descriptions);
  }

}
