<?php

namespace Drupal\guidelines\Access;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\guidelines\Entity\GuidelineInterface;

/**
 * Determines access to guidelines previews.
 */
class GuidelinePreviewAccessCheck implements AccessInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access to the guideline preview page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\guidelines\Entity\GuidelineInterface $guideline_preview
   *   The guideline that is being previewed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, GuidelineInterface $guideline_preview) {
    if ($guideline_preview->isNew()) {
      $access_controller = $this->entityTypeManager->getAccessControlHandler('guideline');
      return $access_controller->createAccess($guideline_preview->bundle(), $account, [], TRUE);
    }
    else {
      return $guideline_preview->access('update', $account, TRUE);
    }
  }

}
