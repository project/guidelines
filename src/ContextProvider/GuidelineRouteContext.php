<?php

namespace Drupal\guidelines\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\guidelines\Entity\Guideline;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Sets the current guideline as a context on guideline routes.
 */
class GuidelineRouteContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new GuidelineRouteContext.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = [];
    $context_definition = EntityContextDefinition::create('guideline')->setRequired(FALSE);
    $value = NULL;
    if (($route_object = $this->routeMatch->getRouteObject())) {
      $route_contexts = $route_object->getOption('parameters');
      // Check for a guideline revision parameter first.
      if (isset($route_contexts['guideline_revision']) && $revision = $this->routeMatch->getParameter('guideline_revision')) {
        $value = $revision;
      }
      elseif (isset($route_contexts['guideline']) && $guideline = $this->routeMatch->getParameter('guideline')) {
        $value = $guideline;
      }
      elseif (isset($route_contexts['guideline_preview']) && $guideline = $this->routeMatch->getParameter('guideline_preview')) {
        $value = $guideline;
      }
      elseif ($this->routeMatch->getRouteName() == 'guideline.add') {
        $guideline_type = $this->routeMatch->getParameter('guideline_type');
        $value = Guideline::create(['type' => $guideline_type->id()]);
      }
    }

    $cacheability = new CacheableMetadata();
    $cacheability->setCacheContexts(['route']);

    $context = new Context($context_definition, $value);
    $context->addCacheableDependency($cacheability);
    $result['guideline'] = $context;

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $context = EntityContext::fromEntityTypeId('guideline', $this->t('Guideline from URL'));
    return ['guideline' => $context];
  }

}
