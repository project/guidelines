<?php

namespace Drupal\guidelines;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for guideline.
 */
class GuidelineTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
