<?php

namespace Drupal\guidelines\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Guideline entities.
 *
 * @ingroup guidelines
 */
class GuidelineDeleteForm extends ContentEntityDeleteForm {


}
